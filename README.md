# Add CI to GitLab Pages

## Create a `.gitlab-ci.yml`'s file 

First, add a docker image (we'll put a "node.js" docker image)

### Create "stages"

We'll make test (with EsLint) and build stages.

### Provide all the scripts

Scripts are define in `package.json`, you can add or change the scripts in this file.

### Stipulate "artefacts"

Our "build" script (`gatsby build`) will create the ".public" folder.

### Specify cache & branch of deployment

Here, cache is on `package.json`'s file and `.nodes_modules`'s folder, and we're going to provide we want deploy on "master" branch.

## Add a --prefix-paths to `gatsby-config.js`

``` pathPrefix: `/affaire-a-suivre-travaux-acces-difficile/`,```

## Add --prefix-paths to scripts

- in `package.json`:
```
  "scripts": {
    "build": "./node_modules/.bin/gatsby build --prefix-paths",
```
- next, in `.gitlab-ci.yml`:
```script:
    - npm install gatsby-cli
    - ./node_modules/.bin/gatsby build --prefix-paths
    - npm run build
```
## IMPORTANT TERMINAL COMMANDE LINE !

Finally, don't forget tu run `gatsby build --prefix-paths` in commande line before commit and push to Gitlab.

You can merge to master branch (if the pipeline is passed), in order to deploy to your Gitlab Pages.

# Installation 

Install the dependencies:

### `yarn install`

Run the development server:

### `yarn dev`

Production build to `/public`:

### `yarn build`

Cleanup cache (often fixes misc errors when run before `yarn dev`):

### `yarn clean`

## Content

Each of the sections in the site are placed in `src/sections`. Data is usually separated out into objects/arrays to be rendered in the component.

## SEO

The component `src/components/common/SEO.js` handles all meta data and SEO content, modify the `SEO_DATA` variable to add the data automatically. For application manifest data and favicon, modify the `gatsby-plugin-manifest` configuration in `gatsby-config.js`.

## Styling

This project uses [styled-components]() to handle styling: `src/styles/theme.js` defines the styling base and `src/styles/GlobalStyles.js` includes basic element styles along with the CSS Reset.
