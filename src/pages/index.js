import React from 'react';

import Layout from '@common/Layout';
import Navbar from '@common/Navbar';

import Header from '@sections/Header';
import About from '@sections/About';
import Brands from '@sections/Brands';
import Team from '@sections/Team';
import Faq from '@sections/Faq';
import Footer from '@sections/Footer';
import ContactForm from '@sections/contactForm';

//import Form from 'react-bootstrap-form';
import 'bootstrap/dist/css/bootstrap.min.css';
//import { Form } from 'react-bootstrap';

//import ReactDOM from "react-dom"
//import App from "./App"
//import "./assets/styles.css"
//import * as serviceWorker from "./serviceWorker"

const IndexPage = () => (
  <Layout>
    <Navbar />
    <Header />
    <About />
    <Brands />
    <Team />
    <Faq />
    <ContactForm />
   
    <Footer />
  </Layout>
);

export default IndexPage;
