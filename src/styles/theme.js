const theme = {
  font: {
    primary: `'Prata', serif`,
    secondary: `'Average', serif`,
  },
  font_size: {
    small: 'font-size: 20px; line-height: 30px',
    regular: 'font-size: 24px; line-height: 32px',
    large: 'font-size: 30px; line-height: 40px',
    larger: 'font-size: 36px; line-height: 48px',
    xlarge: 'font-size: 48px; line-height: 56px',
  },
  color: {
    white: {
      regular: '#FFFFFF',
      dark: '#F6F6F6',
    },
    black: {
      lighter: '#ABA8AF',
      light: '#564F62',
      regular: '#211E26',
    },
    primary: 'rgba(53, 91, 121, 1)',
    /*header {
      background: rgb(66,9,103);rgba(53, 91, 121, 1);
      background: rgba(53, 91, 121, 1);
    
    }*/
    //background: linear-gradient( 0% rgba(14,121,32,1), 50% rgba(53,91,121,1), 100% rgba(53,91,121,1)),
    //secondary: '#438CC4',
    //secondary: '#6f8b9f',
    secondary: '#fff',
  },
  screen: {
    xs: '575px',
    sm: '767px',
    md: '991px',
    lg: '1199px',
  },
};

export default theme;
