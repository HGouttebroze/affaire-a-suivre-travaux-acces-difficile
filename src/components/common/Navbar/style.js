import styled from 'styled-components';

import { Container } from '@components/global';

export const Nav = styled.nav`
  padding: 16px 0;
  background-color: ${props => props.theme.color.secondary};
  position: fixed;
  width: 100%;
  top: 0;
  z-index: 1000;
`;

export const StyledContainer = styled(Container)`
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

export const NavListWrapper = styled.div`
  ul {
    list-style: none;
    margin: 0;
    padding: 0;
    display: flex;
    flex-direction: row;

    ${({ mobile }) =>
      mobile &&
      `
        flex-direction: column;
        margin-top: 1em;

        > ${NavItem} {
          margin: 0;
          margin-top: 0.75em;
        }
      `};
  }
`;

export const NavItem = styled.li`
  margin: 0 0.75em;
  font-family: ${props => props.theme.font.secondary};
  ${props => props.theme.font_size.small};
  z-index: 0;

  a {
    font-family: Arial, Helvetica, sans-serif;
    background-color: black;
    height: 40px;
    width: 120px;
    text-decoration: none; 
    border-radius: 10px;
    opacity: 0.6;
    color: white;
    /*centre le texte ds le cadre */
    display: flex;
    align-items: center;
    justify-content: center;
    /* 1 petite bordure */
    border: 1px solid #892cdc;
    /* on n veut pas afficher le pseudo element, il servira pr le remplissage */
    overflow: hidden;
    /* pr que le pseudo-element (se mette derriere le texte) se superpose par dessus le bouton/lien/cadre */
    z-index: 0;
    position: relative;
}
/* créons le pseudo-element (un rectangle violet va s'afficher sous notre cadre, si on commente la props overflow de l'event a) */
a::before {
    content: '';
    position: absolute;
    top: 60px;
    left: 0;
    width: 100%;
    height: 100%;
    background:  ${props => props.theme.color.primary};
    /* pr l'effet on add 1 border radius en haut à gauche et droite */
    /* pr qu'au survol le pseudo remonte et que ses bodures passent d'un arrondi à 50% à 1 flat à 0 */
    border-radius: 50% 50% 0 0;
    transition: all .5s;
    /* pr placer en dessous du txt */
    z-index: -1;
}

/* detecter le survol du lien et modifier pseudo class before */
a:hover::before{
    /* add background to top */
    top: 0;
    /* on enleve le border radius pr remplir totalement le lien */
    border-radius: 0;
}

/* à ce moment le pseudo element passe de 0 à 60 px vers le haut au survol du lien */


  &.active {
    a {
      opacity: 1;
    }
  }
`;

export const MobileMenu = styled.div`
  width: 100%;
  background: ${props => props.theme.color.primary};
`;

export const Brand = styled.div`
  font-family: ${props => props.theme.font.primary};
  ${props => props.theme.font_size.large};
`;

export const Mobile = styled.div`
  display: none;

  @media (max-width: ${props => props.theme.screen.md}) {
    display: block;
  }

  ${props =>
    props.hide &&
    `
    display: block;

    @media (max-width: ${props.theme.screen.md}) {
      display: none;
    }
  `}
`;
