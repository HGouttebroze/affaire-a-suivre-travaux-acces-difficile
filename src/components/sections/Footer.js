import React from 'react';
import styled from 'styled-components';
import { StaticQuery, graphql } from 'gatsby';
import Img from 'gatsby-image';

import { Container } from '@components/global';
import ExternalLink from '@common/ExternalLink';

//import GithubIcon from '@static/icons/github.svg';
//import InstagramIcon from '@static/icons/instagram.svg';
//import TwitterIcon from '@static/icons/twitter.svg';
import FacebookIcon from '@static/icons/facebook.svg';
import ContactIcon from '@static/icons/CARTE2.svg';

//import { FacebookCircle } from '@styled-icons/boxicons-logos/FacebookCircle;'

const SOCIAL = [
  // {
  //   icon: GithubIcon,
  //   link: 'https://github.com/ajayns/gatsby-absurd',
  // },
  // {
  //   icon: InstagramIcon,
  //   link: 'https://instagram.com/ajay_ns',
  // },

  
  {
    icon: FacebookIcon,
    link: 'https://fr-fr.facebook.com/gerald.magnenant',
  },
  
];

// const LOGOS = () => [
//   {
//     logo: ContactIcon,
//     link: 'https://fr-fr.facebook.com/gerald.magnenant',
//   },
// ];

// Illustrations by
//                 {` `}
//                 <ExternalLink href="https://gitlab.com/HGouttebroze">
//                   @Hugues_G
//                 </ExternalLink>

const Footer = () => (

    <StaticQuery
      query={graphql`
        query {
          allFile(filter: { sourceInstanceName: { eq: "footer" } }) {
            edges {
              node {
                relativePath
                childImageSharp {
                  fluid(maxWidth: 400, maxHeight: 400) {
                    ...GatsbyImageSharpFluid
                  }
                }
              }
            }
          }
         
          
        }
      `}
      
    render={data => (
      <React.Fragment>
        <Art>
          <Img
           
          />
        </Art>
        <FooterWrapper>
          <StyledContainer>
            <Copyright>
              <h2>Affaire à Suivre</h2>
              <span>
                
              </span>
              <footer style={{
                marginTop: `2rem`
                }}>
                  © {new Date().getFullYear()}, Conception & Development by 
                {` `}
              <a href="https://gitlab.com/HGouttebroze">Hugues Simulacre Web Creation</a>
              </footer>
            </Copyright>
            <SocialIcons>
              {SOCIAL.map(({ icon, link }) => (
                <ExternalLink key={link} href={link}>
                  <img src={icon} alt="link" />
                </ExternalLink>
              ))}
            </SocialIcons>

            <LogoGrid>
              
              <img src={ContactIcon} alt="Logo" />
            </LogoGrid>

          </StyledContainer>
        </FooterWrapper>
      </React.Fragment>
    )}
  />
);

const SocialIcons = styled.div`
  display: flex;

  img {
    margin: 0 8px;
    width: 70px;
    height: 70px;
  }

  @media (max-width: ${props => props.theme.screen.sm}) {
    margin-top: 40px;
  }
`;

const LogoGrid = styled.div`
  display: flex;

  img {
    margin: 0 8px;
    width: 300px;
    height: 300px;
  }

  @media (max-width: ${props => props.theme.screen.sm}) {
    margin-top: 40px;
  }
`;

const FooterWrapper = styled.footer`
  background-color: ${props => props.theme.color.primary};
  padding: 32px 0;
`;

const Copyright = styled.div`
  font-family: ${props => props.theme.font.secondary};
  ${props => props.theme.font_size.small};
  color: ${props => props.theme.color.black.regular};

  a {
    text-decoration: none;
    color: inherit;
  }
`;

const Art = styled.figure`
  display: flex;
  justify-content: center;
  margin: 0;
  margin-top: 48px;
`;

const StyledContainer = styled(Container)`
  display: flex;
  justify-content: space-between;
  align-items: center;

  @media (max-width: ${props => props.theme.screen.sm}) {
    flex-direction: column;
    text-align: center;
  }
`;


export default Footer;

// TO DO: mettre l'image de la carte de visite verso avec adresse en SVG pr avoir une transparance & 1 fondu ds le footer
// la retravailler en amond avec INSCKAPE !!!
