import React from 'react';

import { Section, Container } from '@components/global';
//import Form from 'react-bootstrap-form';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Form, Button } from 'react-bootstrap';

class ContactForm extends React.Component {
render() {
  // const formStyle = {
  //   textAlign: 'left'
  // }
  // const buttonsStyle = {
  //   margin: '1rem',
  //   textAlign: 'center',
  //   fontSize: '1em',
  //   padding: '0.25em 1em',
  //   border: '2px solid black',
  //   borderRadius: '3px',
    
  // }
  return (
  <Section>
    <Container>
      <h1>Contacter Affaire A Suivre!</h1>
      <br/>
      <p>N'hésitez pas à nous envoyer un message, pour tous renseignements, problèmes, devis, chantiers, intervenions...
        <br/>
        Nous vous répondrons dans les plus brefs délais.
        <br/>
      </p>
      <br/>
    <Form>
  <Form.Group controlId="formBasicEmail">
    <Form.Label>Email address</Form.Label>
    <Form.Control type="email" placeholder="Enter email" />
    <Form.Text className="text-muted">
      We'll never share your email with anyone else.
    </Form.Text>
  </Form.Group>

  <Form.Group controlId="formBasicPassword">
    <Form.Label>Password</Form.Label>
    <Form.Control type="password" placeholder="Password" />
  </Form.Group>
  <Form.Group controlId="formBasicCheckbox">
    <Form.Check type="checkbox" label="Check me out" />
  </Form.Group>
  <Button variant="primary" type="submit">
    Envoyer
  </Button>
</Form>


    {/* <form style={formStyle} className="form" action={this.props.action} method="post">
      <div>
        <label>Nom</label>
        <input type="text" name="fullname"/>
      </div>
      <div>
        <label>Prenom</label>
        <input type="text" name="lastname"/>
      </div>
      <div>
        <label>Email</label>
        <input type="email" name="email"/>
      </div>
      <div>
        <label>Message</label>
        <textarea name="message" rows="5"></textarea>
      </div>
      <ul className="actions" style={buttonsStyle}>
          <li>
            <button type="submit"  className="button special">Envoyer</button>
          </li>
      </ul>
    </form> */}
    </Container>
    </Section>
  )
}
}

//TO DO : centre le title h1, en passant un scope CSS ds le JSX

export default ContactForm;